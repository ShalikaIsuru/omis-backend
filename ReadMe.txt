
*Open the university-management-system folder from cloned project

*Update your email address and password in application.propertise
	
	-->spring.mail.username=Your Email Address(Sender Email Address)--> e.g.sender@gmail.com
	-->spring.mail.password=Your Email Password(Sender Email Password)

*After that save the changes and run the application 

*In your Gmail account(sender's gmail account),turn on "less secure app access" to login gmail account 
 through the app to send emails

	-->Click on profile icon in gmail(right hand corner)
	-->Go to Manage your Google Account
	-->Search "Less secure app access"in search bar at the top of the page
	-->Click the first option appear
	-->Turn on "Allow less secure apps"

*Send Post request to send notification email and notification sms using postman

	-->send notification email url --> http://localhost:8080/university-management/sendmail
		Body--> KEY-->emailTo
			VALUE-->receiver@gmail.com(receiver email address)
		Body--> KEY-->emailFrom
			VALUE-->sender@gmail.com(sender email address)
		Body--> KEY-->emailSubject
			VALUE-->Notification Email(email subject)
		Body--> KEY-->emailContent
			VALUE-->This is a Notification Email!!!
				Thanks
				OUSL(email content)
					
	-->send notification sms url --> http://localhost:8080/university-management/sendsms
		Body--> KEY-->mobileNumber
			VALUE-->0712345678(receiver mobile number)

(screen_shots_for_testing folder has images for sending notification emails and sending notification sms using "postman")

*Then you can see reponse as a JSON object in the postman console and relavent receiver receive notification email and sms
	(for email send url receive notification email)
	(for sms send url receive notification sms)
